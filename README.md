# Curriculum Vitae Application

## Boundaries

Under the constraint of time, here are some of the boundaries that have been made:

- [Git](https://git-scm.com): It would be preferrable to take full advantage of `git flow` with `master`, `develop`, `hotfix` and `feature` to continuously deploy at all stages.
- [TypeScript](https://www.typescriptlang.org/docs): It would be preferrable to introduce type checking with f.i. TypeScript to continuously deploy with confidence in types being correct at all times.
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro): It would be preferrable to introduce testing with f.i. React Testing Library to continuously deploy with confidence in critical behavior and functionality.

No attention has been given to accessibility, responsiveness, flexibility, animations, theming, internationalization, SEO, high volumes of data, fetch timeouts, error handling and more advanced problems.
The application is currently only optimized for the latest version of Chrome with a resolution of 1366 x 768.

## Introduction

The application has been built with:

- [Node](https://nodejs.org): JavaScript Runtime
- [Yarn](https://yarnpkg.com/en/docs): Dependency Management
- [React](https://reactjs.org/docs): User Interface
- [Create React App](https://facebook.github.io/create-react-app/docs): Build Configuration
- [Reach Router](https://reach.tech/router): Routing
- [Emotion](https://emotion.sh/docs): CSS-Styling
- [Date FNS](https://date-fns.org): Date Utilities

## Installation

You need at least `git v2`, `node v8` and `yarn v1` to install the application. You can verify that you have required versions by running:

```
git --version
node --version
yarn --version
```

After verifying you have the required versions, you can clone and enter the repository by running:

```
git clone --branch develop [git-endpoint]
cd cv-app
```

After cloning the repository, you can install the application by running:

```
yarn install
```

## Development

After installation, you can run the application locally with:

```
yarn start
```

You can see the application running at `http://localhost:3000`

As long as the development server is running, the application should automatically recompile as you change its files.

## Contributing

It is recommended to install `eslint` and `prettier` packages in your editor and align your settings with this project to avoid conflicts at any time.

The application uses `husky` to run a pre-commit hook with `lint-staged` to enforce formatting rules defined by our `eslint` and `prettier` configurations.

## State Management

The application is simple and therefore only uses `useState` which was introduced in React 16.8.

As the application grows, we would introduce the use of `useContext` to accomodate the need for the same state to be used in multiple different places of the application. In case the use of the same state in many places exceeds a certain volume, it would be interesting to look further into a framework like `Relay`.

## Backend Dependency

Currently, the application is only dependent on fake backend requests incorporated in this application to fetch data.

If the application was to be used in a real world environment, it would be highly relevant to look into making a backend application with a `GraphQL` layer in between. `REST APIs` are usually very loosely designed, the structure is not very strict, and the paradigm gives a much greater degree of interpretation for the individual software developer. The structure and architecture of `GraphQL` makes it easier to communicate between the frontend and backend without the need for too much documentation, planning and communication back and forth.

The standard dependence would be for the backend to handle areas such as authentication, tokens, role management, CRUD-operations etc.

## Structure / Patterns

Describe components (stateless function components), reusability and other important aspects.

Describe how for large scale apps, you would use a library like styleguidist to make a style guide that showed off all main components.

## Testing

You can run all tests by using:

```
yarn test
```

## Deployment

### Build

Make sure that the node modules are updated with `yarn install`

You can build the application by running:

```
yarn build
```

After running the build script, the build is available in the `build` folder.

The build is bundled, optimized and minified according to performant industry standards by the configuration of React Scripts.

It is possible to analyze the bundle size and more by taking advantage of `source-map-explorer` and running:

```
yarn analyze
```

### Deploy

There is no deployment strategy at this point. The application is only available on CodeSandbox at this time.
