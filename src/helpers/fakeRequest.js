export default function fakeRequest(data) {
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      resolve({ data })
      clearTimeout(timeout)
    }, 750)
  })
}
