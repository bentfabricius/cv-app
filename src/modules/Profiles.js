import React, { useState, useEffect } from 'react'
import { getProfiles } from 'api'
import { Loading, NavLink, Empty, Input } from 'components'

export default function Profiles() {
  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  const [profiles, setProfiles] = useState([])
  const isEmpty = profiles.length === 0 || isError

  useEffect(() => {
    let isCurrentSearch = true
    async function fetchProfiles() {
      try {
        setIsLoading(true)
        setIsError(false)
        const response = await getProfiles(searchTerm)
        if (isCurrentSearch) {
          setProfiles(response.data)
          setIsLoading(false)
        }
      } catch (error) {
        if (isCurrentSearch) {
          setIsError(true)
          setIsLoading(false)
        }
      }
    }
    fetchProfiles()
    return () => {
      isCurrentSearch = false
    }
  }, [searchTerm])

  return (
    <React.Fragment>
      <Input
        placeholder="Search..."
        value={searchTerm}
        onChange={event => setSearchTerm(event.target.value)}
      />
      {isLoading ? (
        <Loading />
      ) : isEmpty ? (
        <Empty>No profiles found</Empty>
      ) : (
        profiles.map(({ id, name, title }) => (
          <NavLink key={id} to={`/profile/${id}`}>
            <h2 style={{ fontWeight: 700 }}>{name}</h2>
            <h1>{title}</h1>
          </NavLink>
        ))
      )}
    </React.Fragment>
  )
}
