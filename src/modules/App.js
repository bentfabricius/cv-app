import React from 'react'
import { Router } from '@reach/router'
import { AppContainer, Loading, Scroll, Sidebar } from 'components'
import NotFound from './NotFound'

const Profile = React.lazy(() =>
  import(/* webpackChunkName: "Profile" */ './Profile')
)
const Profiles = React.lazy(() =>
  import(/* webpackChunkName: "Profiles" */ './Profiles')
)

export default function App() {
  return (
    <AppContainer>
      <Sidebar>
        <Scroll>
          <React.Suspense fallback="">
            <Profiles />
          </React.Suspense>
        </Scroll>
      </Sidebar>
      <React.Suspense fallback={<Loading />}>
        <Router style={{ flex: 1 }}>
          <Profile path="/profile/:profileId" />
          <NotFound default />
        </Router>
      </React.Suspense>
    </AppContainer>
  )
}
