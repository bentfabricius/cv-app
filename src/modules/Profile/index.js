import React, { useState, useEffect } from 'react'
import { getProfile } from 'api'
import { Content, Loading, Padding, Space, Empty } from 'components'
import Companies from './Companies'
import Projects from './Projects'
import Educations from './Educations'
import Skills from './Skills'

export default function Profile({ profileId }) {
  const [isLoading, setIsLoading] = useState(true)
  const [isError, setIsError] = useState(false)
  const [isEmpty, setIsEmpty] = useState(true)
  const [profile, setProfile] = useState({})
  const { name, companies, projects, educations, skills } = profile
  const hasName = Boolean(name)

  useEffect(() => {
    let isCurrentProfile = true
    async function fetchProfile() {
      try {
        setIsLoading(true)
        setIsError(false)
        const id = parseInt(profileId, 10)
        const response = await getProfile(id)
        if (isCurrentProfile) {
          const profile = response.data
          const { companies, projects, educations, skills } = profile
          const isEmpty =
            companies.length +
              projects.length +
              educations.length +
              skills.length ===
            0
          setProfile(profile)
          setIsEmpty(isEmpty)
          setIsLoading(false)
        }
      } catch (error) {
        if (isCurrentProfile) {
          setIsError(true)
          setIsLoading(false)
        }
      }
    }
    fetchProfile()
    return () => {
      isCurrentProfile = false
    }
  }, [profileId])

  if (isLoading) {
    return <Loading />
  }
  if (isEmpty || isError) {
    return <Empty>No details found {hasName && `for ${name}`}</Empty>
  }
  return (
    <Content>
      <Padding>
        <Companies companies={companies} />
        <Space height={20} />
        <Educations educations={educations} />
        <Space height={20} />
        <Projects projects={projects} />
        <Space height={20} />
        <Skills skills={skills} />
      </Padding>
    </Content>
  )
}
