import React from 'react'
import { format, parseISO } from 'helpers'
import { Headline, Information } from 'components'

export default function Companies({ companies }) {
  return (
    <section>
      <Headline>Companies</Headline>
      {companies.length === 0 ? (
        <p>No companies found</p>
      ) : (
        companies.map(
          ({ id, name, title, start_date, end_date, description }) => {
            const primaryText = `${title} at ${name}`
            const secondaryText = `${start_date &&
              format(parseISO(start_date), 'MMM yyyy')} – ${
              end_date ? format(parseISO(end_date), 'MMM yyyy') : 'Present'
            }`
            return (
              <Information
                key={id}
                primaryText={primaryText}
                secondaryText={secondaryText}
                description={description}
              />
            )
          }
        )
      )}
    </section>
  )
}
