import React from 'react'
import { format, parseISO } from 'helpers'
import { Headline, Information } from 'components'

export default function Educations({ educations }) {
  return (
    <section>
      <Headline>Educations</Headline>
      {educations.length === 0 ? (
        <p>No educations found</p>
      ) : (
        educations.map(
          ({ id, name, title, start_date, end_date, description }) => {
            const primaryText = `${title} at ${name}`
            const secondaryText = `${start_date &&
              format(parseISO(start_date), 'MMM yyyy')} – ${
              end_date ? format(parseISO(end_date), 'MMM yyyy') : 'Present'
            }`
            return (
              <Information
                key={id}
                primaryText={primaryText}
                secondaryText={secondaryText}
                description={description}
              />
            )
          }
        )
      )}
    </section>
  )
}
