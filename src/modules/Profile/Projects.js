import React from 'react'
import { Headline, Information } from 'components'

export default function Projects({ projects }) {
  return (
    <section>
      <Headline>Projects</Headline>
      {projects.length === 0 ? (
        <p>No projects found</p>
      ) : (
        projects.map(({ id, name, technology, description }) => (
          <Information
            key={id}
            primaryText={name}
            secondaryText={technology}
            description={description}
          />
        ))
      )}
    </section>
  )
}
