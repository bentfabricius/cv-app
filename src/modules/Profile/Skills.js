import React from 'react'
import { Headline, Chip, FlexWrap } from 'components'

export default function Skills({ skills }) {
  return (
    <section>
      <Headline>Skills</Headline>
      <FlexWrap>
        {skills.length === 0 ? (
          <p>No skills found</p>
        ) : (
          skills.map(({ id, name }) => (
            <Chip key={id}>
              <h1 style={{ fontSize: 14 }}>{name}</h1>
            </Chip>
          ))
        )}
      </FlexWrap>
    </section>
  )
}
