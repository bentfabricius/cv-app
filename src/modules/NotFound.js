import React from 'react'
import { Empty } from 'components'

export default function NotFound() {
  return <Empty>Welcome to Curriculum Vitae</Empty>
}
