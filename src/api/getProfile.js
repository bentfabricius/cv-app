import { fakeRequest } from 'helpers'
import all_profiles from './data/profiles.json'
import all_companies from './data/companies.json'
import all_projects from './data/projects.json'
import all_educations from './data/educations.json'
import all_skills from './data/skills.json'

export default function getProfile(id) {
  const profile = all_profiles.find(profile => profile.id === id)
  const companies = all_companies.filter(company => company.profile_id === id)
  const projects = all_projects.filter(project => project.profile_id === id)
  const educations = all_educations.filter(project => project.profile_id === id)
  const skills = all_skills.filter(project => project.profile_id === id)
  return fakeRequest({
    ...profile,
    companies,
    projects,
    educations,
    skills
  })
}
