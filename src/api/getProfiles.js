import { fakeRequest } from 'helpers'
import all_profiles from './data/profiles.json'

export default function getProfiles(searchTerm) {
  const profiles = all_profiles
    .filter(profile =>
      profile.name.toLowerCase().includes(searchTerm.toLowerCase())
    )
    .map(({ id, name, title }) => ({
      id,
      name,
      title
    }))
  if (!profiles) {
    throw Error('No profiles found')
  }
  return fakeRequest(profiles)
}
