import { styled } from 'helpers'

const Space = styled.div(({ height = 1, width = 1 }) => ({
  height,
  width
}))

export default Space
