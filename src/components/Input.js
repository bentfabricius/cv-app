import { styled } from 'helpers'

const Input = styled.input({
  width: '100%',
  height: 70,
  border: 0,
  outline: 0,
  paddingLeft: 25,
  paddingRight: 25,
  backgroundColor: 'rgba(0,0,0,0)',
  transition: 'all 250ms',
  ':hover': {
    backgroundColor: 'rgba(0,0,0,0.06)'
  },
  ':focus': {
    backgroundColor: 'rgba(0,0,0,0.06)'
  },
  '::placeholder': {
    color: 'rgba(0,0,0,0.5)'
  }
})

export default Input
