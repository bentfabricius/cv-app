import { styled } from 'helpers'

const Headline = styled.h1({
  fontWeight: 700,
  marginLeft: 5
})

export default Headline
