import React from 'react'
import { Link } from '@reach/router'
import { styled } from 'helpers'

export default function NavLink(props) {
  return (
    <StyledLink
      {...props}
      getProps={({ isCurrent }) => {
        return {
          style: {
            ...(isCurrent && {
              backgroundColor: '#fff',
              boxShadow: '0 0 10px 0 rgba(0,0,0,0.05)',
              borderLeft: '5px solid green'
            })
          }
        }
      }}
    />
  )
}

const StyledLink = styled(Link)({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  height: 75,
  paddingLeft: 25,
  paddingRight: 25,
  outline: 0,
  transition: 'all 250ms',
  ':hover, :focus': {
    backgroundColor: '#fff',
    boxShadow: '0 0 10px 0 rgba(0,0,0,0.05)',
    borderLeft: '5px solid darkorange'
  }
})
