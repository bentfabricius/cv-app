import { styled } from 'helpers'

const FlexAlignCenter = styled.div({
  display: 'flex',
  alignItems: 'center',
  userSelect: 'none'
})

export default FlexAlignCenter
