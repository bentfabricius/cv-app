import { styled } from 'helpers'

const Scroll = styled.div({
  position: 'absolute',
  top: 0,
  bottom: 0,
  left: 0,
  right: -17,
  overflowY: 'scroll',
  overflowX: 'hidden'
})

export default Scroll
