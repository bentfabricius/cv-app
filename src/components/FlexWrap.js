import { styled } from 'helpers'

const FlexWrap = styled.div({
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center'
})

export default FlexWrap
