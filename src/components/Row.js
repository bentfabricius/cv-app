import { styled } from 'helpers'

const Row = styled.div(({ isOpen }) => ({
  marginTop: isOpen ? 25 : 5,
  marginBottom: isOpen ? 25 : 5,
  borderRadius: 4,
  padding: '6px 12px',
  transition: 'border 150ms',
  border: '1px solid rgba(0,0,0,0.075)',
  ':hover': {
    cursor: 'pointer',
    border: '1px solid darkorange'
  }
}))

export default Row
