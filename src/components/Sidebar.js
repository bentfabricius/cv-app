import { styled } from 'helpers'

const Sidebar = styled.nav({
  height: '100%',
  width: 300,
  overflow: 'hidden',
  position: 'relative',
  backgroundColor: 'rgb(243,247,248)'
})

export default Sidebar
