import { styled } from 'helpers'

const Chip = styled.div({
  border: '1px solid rgba(0,0,0,0.075)',
  borderRadius: 4,
  padding: '6px 12px',
  marginTop: 5,
  marginRight: 5
})

export default Chip
