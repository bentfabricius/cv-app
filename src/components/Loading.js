import React from 'react'
import { Empty } from 'components'

export default function Loading() {
  return <Empty>Loading...</Empty>
}
