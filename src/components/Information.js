import React, { useState } from 'react'
import { Row, Space, FlexAlignCenter } from 'components'

export default function Information({
  primaryText,
  secondaryText,
  description
}) {
  const [isOpen, setIsOpen] = useState(false)
  return (
    <Row isOpen={isOpen} onClick={() => setIsOpen(open => !open)}>
      <FlexAlignCenter>
        <h1 style={{ flex: 1, fontSize: 14 }}>{primaryText}</h1>
        <h2 style={{ fontSize: 14 }}>{secondaryText}</h2>
        <p
          style={{
            width: 65,
            fontSize: 10,
            fontWeight: 700,
            marginLeft: 20
          }}
        >
          {isOpen ? 'Hide details' : 'Show details'}
        </p>
      </FlexAlignCenter>
      {isOpen && (
        <React.Fragment>
          <Space height={15} />
          <h3 style={{ fontWeight: 700, fontSize: 11 }}>Details</h3>
          <p style={{ fontSize: 12 }}>
            {/* {description ? description : 'No details found'} */}
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <Space height={10} />
          <p style={{ fontSize: 12 }}>
            Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
            cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum.
          </p>
        </React.Fragment>
      )}
    </Row>
  )
}
