import { styled } from 'helpers'

const AppContainer = styled.div({
  height: '100%',
  width: '100%',
  display: 'flex'
})

export default AppContainer
