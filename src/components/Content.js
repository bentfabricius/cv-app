import { styled } from 'helpers'

const Content = styled.main({
  flex: 1,
  height: '100%',
  overflow: 'hidden',
  overflowY: 'auto',
  position: 'relative'
})

export default Content
